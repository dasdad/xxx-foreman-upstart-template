Foreman Upstart
===============

[Foreman][1] template for [Upstart][2] that fix [termination issue][3]. Based
on comment by [@jarthoad][4].

Usage:

```
$ foreman export upstart /etc/init \
    --app app/<application> \
    --user app \
    --log /var/log/app/<application> \
    --template <path-to-this-repository>
```


[1]: https://github.com/ddollar/foreman
[2]: http://upstart.ubuntu.com/
[3]: https://github.com/ddollar/foreman/issues/97
[4]: https://github.com/jarthod
